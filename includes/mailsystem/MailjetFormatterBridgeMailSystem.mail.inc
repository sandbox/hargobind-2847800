<?php
class MailjetFormatterBridgeMailSystem implements MailSystemInterface {
  protected $formatClass;
  protected $mailClass;

  public function __construct() {
    // Get the chosen formatter.
    $format_class = variable_get('mfb_format_class', MFB_DEFAULT_CLASS);
    if (drupal_autoload_class($format_class)) {
      $this->formatClass = new $format_class;
    }
    else {
      watchdog('mailjet_formatter_bridge', 'Error loading mail system class for @type() method.', array('@type' => 'format'), WATCHDOG_ERROR);
      $this->formatClass = new DefaultMailSystem;
    }

    // Get the chosen mailer.
    $mail_class = variable_get('mfb_mail_class', MFB_DEFAULT_CLASS);
    if (drupal_autoload_class($mail_class)) {
      $this->mailClass = new $mail_class;
    }
    else {
      watchdog('mailjet_formatter_bridge', 'Error loading mail system class for @type() method.', array('@type' => 'mail'), WATCHDOG_ERROR);
      $this->mailClass = new DefaultMailSystem;
    }
  }

  public function format(array $message) {
    // If the "no_format" parameter is passed, perform the most basic
    // formatting to prepare for sending.
    if (!empty($message['params']['no_format'])) {
      // Join the body array into one string.
      if (is_array($message['body'])) {
        $message['body'] = implode("\n\n", $message['body']);
      }
      // Wrap the mail body for sending.
      $message['body'] = drupal_wrap_mail($message['body']);
      return $message;
    }
    else {
      return $this->formatClass->format($message);
    }
  }

  public function mail(array $message) {
    // Display the email using Devel module.
    if (variable_get('mfb_devel_display', FALSE) && function_exists('dpm')) {
      $devel_msg = array();
      $devel_msg[t('Subject')] = $message['subject'];
      $devel_msg[t('From')] = $message['from'];
      $devel_msg[t('To')] = $message['to'];
      $devel_msg[t('Reply-To')] = isset($message['reply_to']) ? $message['reply_to'] : NULL;
      $devel_msg[t('Header')] = $message['headers'];
      $devel_msg[t('Body')] = $message['body'];

      dpm($devel_msg, 'mailjet_formatter_bridge');
    }

    // Send the email.
    if (variable_get('mfb_send_emails', TRUE)) {
      return $this->mailClass->mail($message);
    }
    // Sending is disabled. Display a message for site admins.
    elseif (user_access('administer site settings')) {
      $message = t('Sending of email messages is disabled by the Mailjet Formatter Bridge module. Go <a href="@href">here</a> to enable.',
        array('@href' => url('admin/config/system/mailjet_formatter_bridge'))
      );

      drupal_set_message($message, 'warning', TRUE);
    }
    // Sending is disabled. Log a message for non-admins.
    else {
      watchdog('mailjet_formatter_bridge', 'Attempted to send an email, but sending emails is disabled.');
    }
    return isset($result) ? $result : TRUE;
  }
}
