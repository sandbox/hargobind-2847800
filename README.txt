
Bridges an email formatting module (HTML Mail or Mime Mail) with MailJet's
sending capabilities.

Drupal's mail system has two primary functions when acting on an email:
*format* the email and *mail* the email. MailJet is a great system for sending
emails, but those emails are still formatted in plain text.

What if you want fancy formatted emails? There are many great modules out there
for formatting emails such as HTML Mail or Mime Mail which have their own
*format* function.

This module creates a bridge between the *format* function of those modules, and
the *mail* function of MailJet. It just performs a minor feature of the more
extensive Mail System module, i.e. the "New Class" generator. The generator was
not working with my symlinked directories, so this module is really just a place
holder for the generated files.


Installation
============

It is suggested that you first install either the HTML Mail or Mime Mail
modules before installing this one. If you install this one first, you can
disable and reenable this module to get it working.

Once you install this module, an attempt will be made to detect if you have one
of the above modules installed, and will update Drupal's "mail system"
configuration with the appropriate formatter.


Using the Mail System (MS) module
=================================

The MS module lets you choose which classes you want to use for Drupal's mail
system. This module registers the mail system classes with the name
XXX__MailjetSmtpMailSystem where XXX is the name of the two formatter modules.
Visit the MS configuration page to change the MailSystemInterface class to the
one you want.
